import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:sushi/bloc/auth/auth_repository.dart';

part 'auth_event.dart';
part 'auth_state.dart';

class AuthBloc extends Bloc<AuthEvent, AuthState> {
  final AuthRepository authRepo;
  AuthBloc({@required this.authRepo}) : super(AuthInitial()) {
    on<VerifyPhoneNumberToGetOtpCode>((event, emit) async {
      try {
        emit(AuthInProgress());
        await authRepo.verifyPhoneNumber(event.phoneNumber);
        emit(PhoneSentWaitForOtp());
      } on FirebaseException catch (error) {
        print(error);
        emit(AuthError());
        rethrow;
      } finally {}
    });

    on<LogIn>((event, emit) async {
      await _login(event, emit);
    });

    //
    // on<LogOut>((event, emit) {
    //   _logOut(event, emit);
    // });
  }

  Future<void> _login(LogIn event, Emitter<AuthState> emit) async {
    try {
      emit(AuthInProgress());
      await authRepo.signInWithPhoneNumber(event.otpCode);
      emit(Authenticated());
    } on FirebaseException catch (error) {
      print(error);
      emit(AuthError());
      rethrow;
    } finally {}
  }

  Future<void> _logOut(LogOut event, Emitter<AuthState> emit) async {
    try {
// ...
    } on Object catch (error, stackTrace) {
//...
      rethrow;
    } finally {}
  }
}
