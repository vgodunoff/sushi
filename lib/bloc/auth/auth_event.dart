part of 'auth_bloc.dart';

abstract class AuthEvent extends Equatable {
  const AuthEvent();
}

class VerifyPhoneNumberToGetOtpCode extends AuthEvent {
  final String phoneNumber;

  VerifyPhoneNumberToGetOtpCode({@required this.phoneNumber});
  @override
  List<Object> get props => [phoneNumber];
}

class LogIn extends AuthEvent {
  final String otpCode;

  LogIn({@required this.otpCode});
  @override
  List<Object> get props => [otpCode];
}

class LogOut extends AuthEvent {
  @override
  List<Object> get props => [];
}
