import 'package:firebase_auth/firebase_auth.dart';

class AuthRepository {
  final FirebaseAuth _auth;
  String _verificationId;

  AuthRepository({FirebaseAuth firebaseAuth})
      : _auth = firebaseAuth ?? FirebaseAuth.instance;

  //methods
  Future<void> verifyPhoneNumber(String phoneNumber) async {
// сначала опишем кол-бэки. они будут использованы ниже

// кол-бэк verificationCompleted отрабатывает когда подтвержден смс-код
    /* то есть сначала юзер вводит номер телефона, затем жмет кнопку и этот
    номер отправляется на сервер
  прилетает с сервера смс. если автоматически смс не проверился андроидом,
  то просим пользователя
  ввести этот смс и нажать кнопку - проверить
  когда проверка закончена и срабатывает кол-бэк verificationCompleted
  */
// PhoneAuthCredential phoneAuthCredential поставляется файербэйзом автоматически
    PhoneVerificationCompleted verificationCompleted =
        (PhoneAuthCredential phoneAuthCredential) async {
      await _auth.signInWithCredential(phoneAuthCredential);
      print(
          "Phone number automatically verified and user signed in: ${_auth.currentUser.uid}");
    };
// кол-бэк от сглаза
    PhoneVerificationFailed verificationFailed =
        (FirebaseAuthException authException) {
      print(
          'Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}');
    };

    //Callback for when the code is sent
// String verificationId также прилетает от файербейза
//когда отработает этот кол-бэк то мы получим  от файербейза verificationId
// и запишем его в локальную переменную _verificationId
//  _verificationId используется для получения учетки в signInWithPhoneNumber
    PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
      _verificationId = verificationId;
      print('This is callback codeSent: '
          'Please check your phone for the verification code.'
          'verificationId from Firebase: $verificationId,'
          'local variable _verificationId: $_verificationId');
    };

    PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      //print("verification code: " + verificationId);
      _verificationId = verificationId;
      print('This is callback codeAutoRetrievalTimeout: '
          'Please check your phone for the verification code.'
          'verificationId from Firebase: $verificationId,'
          'local variable _verificationId: $_verificationId');
    };

    try {
      await _auth.verifyPhoneNumber(
          phoneNumber: phoneNumber,
          timeout: const Duration(seconds: 5),
          verificationCompleted: verificationCompleted,
          verificationFailed: verificationFailed,
          codeSent: codeSent,
          codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
    } catch (e) {
      print("Failed to Verify Phone Number: $e");
    }
  }

  Future<void> signInWithPhoneNumber(String smsCode) async {
    try {
      final AuthCredential credential = PhoneAuthProvider.credential(
        verificationId: _verificationId,
        smsCode: smsCode,
      );

      final User user = (await _auth.signInWithCredential(credential)).user;

      print("Successfully signed in UID: ${user.uid}");
    } catch (e) {
      print("Failed to sign in: " + e.toString());
    }
  }
}
