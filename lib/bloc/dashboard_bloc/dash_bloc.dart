import 'dart:async';

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/cupertino.dart';
import 'package:sushi/repository/constants.dart';
import 'package:sushi/repository/repository.dart';
import 'package:sushi/ui/screens/cart.dart';
import 'package:sushi/ui/screens/main_page/menu/goods.dart';

import 'package:sushi/ui/screens/my_orders.dart';
import 'package:sushi/ui/screens/settings.dart';

part 'dash_event.dart';
part 'dash_state.dart';

class DashBloc extends Bloc<DashEvent, DashState> {
  final repo = Repository();
  var collection = 'Сеты';

  DashBloc() : super(DashInitial()) {
    on<ShowPage>((event, emit) {
      if (event.currentSelected == 0) {
        emit(
          FirstPageSelected(
              currentIndex: 0,
              collectionStream: repo.data[collection],
              title: collection),
        );
      }

      if (event.currentSelected == 1) {
        emit(SecondPageSelected(currentIndex: event.currentSelected));
      }
      if (event.currentSelected == 2) {
        emit(ThirdPageSelected(currentIndex: event.currentSelected));
      }
      if (event.currentSelected == 3) {
        emit(FourthPageSelected(currentIndex: event.currentSelected));
      }
    });
    on<ChangeFirstPage>((event, emit) {
      collection = event.title;
      emit(
        FirstPageSelected(
            currentIndex: 0,
            collectionStream: repo.data[collection],
            title: collection),
      );
    });
  }
}
