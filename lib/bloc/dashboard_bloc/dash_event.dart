part of 'dash_bloc.dart';

abstract class DashEvent extends Equatable {
  const DashEvent();
}

class ShowPage extends DashEvent {
  final int currentSelected;
  final String collection;

  ShowPage({@required this.currentSelected, this.collection});

  @override
  List<Object> get props => [currentSelected];
}

class ChangeFirstPage extends DashEvent {
  final Stream collectionStream;
  final String title;

  ChangeFirstPage({@required this.collectionStream, @required this.title});

  @override
  List<Object> get props => [collectionStream, title];
}
