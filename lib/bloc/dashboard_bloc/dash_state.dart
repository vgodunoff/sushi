part of 'dash_bloc.dart';

abstract class DashState extends Equatable {
  const DashState();
}

class DashInitial extends DashState {
  @override
  List<Object> get props => [];
}

class FirstPageSelected extends DashState {
  //final Widget page;
  final int currentIndex;
  final Stream collectionStream;
  final String title;

  FirstPageSelected(
      {@required this.collectionStream,
      @required this.title,
      @required this.currentIndex});

  @override
  List<Object> get props => [currentIndex, collectionStream, title];
}

class SecondPageSelected extends DashState {
  final int currentIndex;

  SecondPageSelected({@required this.currentIndex});
  @override
  // TODO: implement props
  List<Object> get props => [currentIndex];
}

class ThirdPageSelected extends DashState {
  final int currentIndex;

  ThirdPageSelected({@required this.currentIndex});
  @override
  // TODO: implement props
  List<Object> get props => [currentIndex];
}

class FourthPageSelected extends DashState {
  final int currentIndex;

  FourthPageSelected({@required this.currentIndex});
  @override
  // TODO: implement props
  List<Object> get props => [currentIndex];
}
