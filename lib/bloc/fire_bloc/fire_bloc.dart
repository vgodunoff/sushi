import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:firebase_core/firebase_core.dart';

part 'fire_event.dart';
part 'fire_state.dart';

class FireBloc extends Bloc<FireEvent, FireState> {
  FireBloc() : super(FireInitial()) {
    on<FetchData>((event, emit) async {
      try {
        await Firebase.initializeApp();
        emit(Loaded());
      } catch (e) {
        print(e);
      }
    });
  }
}
