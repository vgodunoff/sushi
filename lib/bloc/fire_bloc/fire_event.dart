part of 'fire_bloc.dart';

abstract class FireEvent extends Equatable {
  const FireEvent();
}

class FetchData extends FireEvent {
  @override
  List<Object> get props => [];
}
