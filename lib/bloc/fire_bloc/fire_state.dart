part of 'fire_bloc.dart';

abstract class FireState extends Equatable {
  const FireState();
}

class FireInitial extends FireState {
  @override
  List<Object> get props => [];
}

class Loaded extends FireState {
  @override
  List<Object> get props => [];
}
