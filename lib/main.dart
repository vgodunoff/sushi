import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
//import 'package:sms_autofill/sms_autofill.dart';
import 'package:sushi/ui/screens/admin.dart';
import 'package:sushi/ui/screens/authenticated_screen.dart';
import 'package:sushi/ui/screens/cart.dart';
import 'package:sushi/ui/screens/my_orders.dart';
import 'package:sushi/ui/screens/phone_login.dart';
import 'package:sushi/ui/screens/phone_login_screen.dart';
import 'package:sushi/ui/screens/phone_login_screen_stfl.dart';
import 'package:sushi/ui/screens/settings.dart';
import 'package:sushi/ui/widgets/logo_screen.dart';
import 'package:sushi/ui/widgets/widgets_home_page/home_page_dashboard.dart';

import 'bloc/fire_bloc/fire_bloc.dart';

Future<void> main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: Colors.red, // status bar color
    statusBarIconBrightness: Brightness.light, // status bar icon color
    systemNavigationBarIconBrightness:
        Brightness.light, // color of navigation controls
  ));
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      theme: ThemeData(
        primaryColor: Colors.red,
        appBarTheme: AppBarTheme(color: Colors.red),
        colorScheme: ColorScheme.fromSwatch().copyWith(secondary: Colors.red),
      ),
      routes: {
        Cart.routeName: (context) => Cart(),
        MyOrders.routeName: (context) => MyOrders(),
        Settings.routeName: (context) => Settings(),
        LoginScreen.routeName: (context) => LoginScreen(),
        AuthenticatedPage.pageName: (context) => AuthenticatedPage(),
        //PhoneAuth.routeName: (context) => PhoneAuth(),
        Admin.routeName: (context) => Admin(),
      },
      home: AuthenticatedPage(),
      //LoginScreen(),
      // DashboardHomeScreen(),
      // BlocProvider(
      //   create: (context) => FireBloc()..add(FetchData()),
      //   child: DashboardHomeScreen(),
      // ),
    );
  }
}
