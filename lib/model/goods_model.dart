import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class GoodsModel {
  final Stream<QuerySnapshot<Map<String, dynamic>>> collection;
  final String title;

  GoodsModel({@required this.collection, @required this.title});
}
