import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:sushi/model/goods_model.dart';
import 'package:sushi/repository/constants.dart';

class Repository {
  static final sets = GoodsModel(collection: collectionSet, title: 'Сеты');
  static final rolls = GoodsModel(collection: collectionRoll, title: 'Роллы');

  List<GoodsModel> goodsModels = <GoodsModel>[sets, rolls];
  static Stream collectionSet =
      FirebaseFirestore.instance.collection('rollset').snapshots();
  static Stream collectionRoll =
      FirebaseFirestore.instance.collection('singleRoll').snapshots();

  static const String setString = 'Сеты';

  static const String rollString = 'Роллы';

  Map<String, Stream<QuerySnapshot<Map<String, dynamic>>>> data = {
    setString: collectionSet,
    rollString: collectionRoll
  };
}
