import 'package:flutter/material.dart';
import 'package:sushi/ui/widgets/alert_dialog_choice_city.dart';

class AuthenticatedPage extends StatelessWidget {
  static const String pageName = '/loggedIn';
  const AuthenticatedPage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          children: [
            ChoiceCity(),
            Container(
              child: Text('Congratulations!!!'),
            ),
          ],
        ),
      ),
    );
  }
}
