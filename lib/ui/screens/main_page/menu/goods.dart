import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:sushi/ui/screens/main_page/menu/goods_detail.dart';

import 'package:sushi/ui/widgets/buttons/button_favorite.dart';
import 'package:sushi/ui/widgets/buttons/button_want.dart';
import 'package:sushi/ui/widgets/card_product/product_image.dart';
import 'package:sushi/ui/widgets/card_product/product_name_favorite.dart';
import 'package:sushi/ui/widgets/card_product/product_price_order.dart';

class Goods extends StatelessWidget {
  final Stream collectionStream;
  final String title;

  Goods({this.collectionStream, this.title});
  @override
  Widget build(BuildContext context) {
    print(collectionStream);
    return StreamBuilder<QuerySnapshot>(
      stream: collectionStream,
      builder: (BuildContext context, AsyncSnapshot<QuerySnapshot> snapshot) {
        if (snapshot.hasError) {
          return Text('Something went wrong ${snapshot.error}');
        }

        if (snapshot.connectionState == ConnectionState.waiting) {
          return Text("Loading");
        }

        return Scaffold(
          appBar: AppBar(
            leadingWidth: 100,
            leading: Align(
                alignment: Alignment.center,
                child: Text(
                  title,
                  style: TextStyle(fontSize: 20),
                )),

            actions: [
              ButtonFavorite(
                inAppBar: true,
              ),
              SizedBox(
                width: 4.0,
              ),
              Icon(Icons.search, size: 27),
              SizedBox(
                width: 8.0,
              ),
            ],
            // centerTitle: false,
          ),
          body: ListView(
            children: snapshot.data.docs.map((DocumentSnapshot document) {
              Map<String, dynamic> product =
                  document.data() as Map<String, dynamic>;

              return Card(
                child: InkWell(
                  onTap: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return GoodsDetail(
                        data: product,
                      );
                    }));
                  },
                  child: Padding(
                    padding: const EdgeInsets.only(left: 8),
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: [
                        ProductImage(
                          productData: product,
                          isDetail: false,
                        ),
                        Expanded(
                          child: Padding(
                            padding: const EdgeInsets.all(8.0),
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.stretch,
                              children: [
                                ProductNameAndFavorite(productData: product),
                                SizedBox(
                                  height: 15.0,
                                ),
                                ProductPriceOrder(
                                  productData: product,
                                ),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              );
            }).toList(),
          ),
        );
      },
    );
  }
}
