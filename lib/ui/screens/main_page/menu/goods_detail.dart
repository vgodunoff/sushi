import 'package:flutter/material.dart';
import 'package:sushi/ui/widgets/buttons/button_want.dart';
import 'package:sushi/ui/widgets/card_product/product_image.dart';
import 'package:sushi/ui/widgets/card_product/product_name_favorite.dart';

class GoodsDetail extends StatelessWidget {
  final Map<String, dynamic> data;

  const GoodsDetail({this.data});
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(),
      body: Container(
        padding: EdgeInsets.symmetric(vertical: 8, horizontal: 14),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: [
            ProductImage(
              productData: data,
              isDetail: true,
            ),
            SizedBox(
              height: 8,
            ),
            ProductNameAndFavorite(
              productData: data,
            ),
            SizedBox(
              height: 14,
            ),
            Text(data['description']),
            Spacer(
              flex: 5,
            ),
            Row(
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Text(
                      'Цена:',
                      style: TextStyle(fontSize: 15),
                    ),
                    SizedBox(
                      height: 4,
                    ),
                    Text(
                      '${data['price'].toString()},00 тг.',
                      style: TextStyle(
                          // color: Colors.red,
                          fontWeight: FontWeight.w500,
                          fontSize: 18),
                    ),
                  ],
                ),
                WantButton(
                  smallButton: false,
                ),
              ],
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
            ),
          ],
        ),
      ),
    );
  }
}
