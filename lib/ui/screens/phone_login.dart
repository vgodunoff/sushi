// import 'package:firebase_auth/firebase_auth.dart';
// import 'package:flutter/cupertino.dart';
// import 'package:flutter/material.dart';
// import 'package:sms_autofill/sms_autofill.dart';
// import 'package:sushi/ui/widgets/alert_dialog_choice_city.dart';

// class PhoneAuth extends StatefulWidget {
//   static const routeName = '/profile';
//   static const String prefixPhone = '+7';

//   @override
//   _PhoneAuthState createState() => _PhoneAuthState();
// }

// class _PhoneAuthState extends State<PhoneAuth> {
//   final FirebaseAuth _auth = FirebaseAuth.instance;

//   final _scaffoldKey = GlobalKey<ScaffoldState>();

//   final TextEditingController _phoneNumberController = TextEditingController();
//   final TextEditingController _smsController = TextEditingController();
//   String _verificationId;
//   final SmsAutoFill _autoFill = SmsAutoFill();

//   String fullNumber = '';

//   var controller = TextEditingController();

//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//         appBar: AppBar(
//           title: Text('Авторизация'),
//         ),
//         backgroundColor: Colors.white,
//         body: Padding(
//           padding: const EdgeInsets.fromLTRB(8.0, 16.0, 8.0, 4.0),
//           child: Column(
//             children: [
//               Padding(
//                 padding: const EdgeInsets.all(8.0),
//                 child: Text('Введите Ваш номер телефона'),
//               ),
//               TextField(
//                 textAlignVertical: TextAlignVertical.bottom,
//                 keyboardType: TextInputType.phone,
//                 maxLength: 10,
//                 autofocus: true,
//                 decoration: InputDecoration(
//                   isDense: true,
//                   icon: Text(
//                     'Казахстан ${PhoneAuth.prefixPhone}',
//                     style: TextStyle(color: Colors.red, fontSize: 16),
//                   ),
//                   filled: true,
//                   fillColor: Colors.grey[100],
//                   border: InputBorder.none,
//                 ),
//                 cursorColor: Colors.red,
//                 style: TextStyle(color: Colors.red),
//                 controller: controller,
//               ),
//               FlatButton(
//                 color: Colors.red,
//                 onPressed: () async {
//                   fullNumber = PhoneAuth.prefixPhone + controller.text;
//                   debugPrint(fullNumber);
//                   showDialogChoiceCity(context);
//                 },
//                 child: Text(
//                   'Вход',
//                   style: TextStyle(color: Colors.white),
//                 ),
//                 minWidth: MediaQuery.of(context).size.width * 0.8,
//                 shape: RoundedRectangleBorder(
//                   borderRadius: BorderRadius.circular(12.0),
//                 ),
//               ),
//             ],
//           ),
//         ));
//   }

//   Future<void> showDialogChoiceCity(BuildContext context) async {
//     // return await showDialog(
//     //     context: context,
//     //     builder: (context) {
//     //       return Choice
//     //       City();
//     //     });
//   }
// }
