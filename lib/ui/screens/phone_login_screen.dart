import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:sushi/bloc/auth/auth_bloc.dart';
import 'package:sushi/bloc/auth/auth_repository.dart';
import 'package:sushi/ui/screens/authenticated_screen.dart';
import 'package:sushi/ui/widgets/alert_dialog_choice_city.dart';

class LoginScreen extends StatelessWidget {
  static const routeName = '/profile';
  static const String prefixPhone = '+1';

  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _smsController = TextEditingController();
  //final SmsAutoFill _autoFill = SmsAutoFill();
  final AuthRepository repository = AuthRepository();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
        create: (context) => AuthBloc(authRepo: repository),
        child: Scaffold(
            resizeToAvoidBottomInset: false,
            appBar: AppBar(
              title: Text('Авторизация'),
            ),
            backgroundColor: Colors.white,
            body: BlocConsumer<AuthBloc, AuthState>(
              listener: (context, state) {
                if (state is AuthInProgress) {
                  return Center(child: CircularProgressIndicator());
                }
                if (state is Authenticated) {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (BuildContext context) =>
                              AuthenticatedPage()));
                }
              },
              builder: (context, state) {
                if (state is AuthInitial) {
                  return Padding(
                    padding: const EdgeInsets.fromLTRB(8.0, 16.0, 8.0, 4.0),
                    child: Column(
                      children: [
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text('Введите Ваш номер телефона'),
                        ),
                        TextField(
                          textAlignVertical: TextAlignVertical.bottom,
                          keyboardType: TextInputType.phone,
                          maxLength: 10,
                          autofocus: true,
                          decoration: InputDecoration(
                            isDense: true,
                            icon: Text(
                              'Казахстан ${LoginScreen.prefixPhone}',
                              style: TextStyle(color: Colors.red, fontSize: 16),
                            ),
                            filled: true,
                            fillColor: Colors.grey[100],
                            border: InputBorder.none,
                          ),
                          cursorColor: Colors.red,
                          style: TextStyle(color: Colors.red),
                          controller: _phoneNumberController,
                        ),
                        // ElevatedButton(
                        //   onPressed: () async {
                        //     _phoneNumberController.text = await _autoFill.hint;
                        //   },
                        //   child: Text('Get currrent number'),
                        // ),
                        TextButton(
                          style: TextButton.styleFrom(
                            backgroundColor: Colors.red,
                            shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(12.0),
                            ),
                            minimumSize: Size(
                                MediaQuery.of(context).size.width * 0.8, 36),
                          ),
                          onPressed: () async {
                            String fullNumber = LoginScreen.prefixPhone +
                                _phoneNumberController.text;
                            BlocProvider.of<AuthBloc>(context).add(
                                VerifyPhoneNumberToGetOtpCode(
                                    phoneNumber: fullNumber));
                            // showDialogChoiceCity(context);
                            //verifyPhoneNumber();
                          },
                          child: Text(
                            'Verify number',
                            style: TextStyle(color: Colors.white),
                          ),
                        ),
                      ],
                    ),
                  );
                }
                if (state is PhoneSentWaitForOtp) {
                  return Center(
                    child: Column(
                      children: [
                        TextFormField(
                          keyboardType: TextInputType.phone,
                          controller: _smsController,
                          decoration: const InputDecoration(
                              labelText: 'Verification code'),
                        ),
                        Container(
                          padding: const EdgeInsets.only(top: 16.0),
                          alignment: Alignment.center,
                          child: ElevatedButton(
                              onPressed: () async {
                                BlocProvider.of<AuthBloc>(context)
                                    .add(LogIn(otpCode: _smsController.text));
                                //signInWithPhoneNumber();
                              },
                              child: Text("Sign in")),
                        ),
                      ],
                    ),
                  );
                }

                return Container();
              },
            )));
  }
}

// Future<void> showDialogChoiceCity(BuildContext context) async {
//   return await showDialog(
//       context: context,
//       builder: (context) {
//         return ChoiceCity();
//       });
// }

class SignInWithOtp extends StatelessWidget {
  final TextEditingController _smsController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextFormField(
          keyboardType: TextInputType.phone,
          controller: _smsController,
          decoration: const InputDecoration(labelText: 'Verification code'),
        ),
        Container(
          padding: const EdgeInsets.only(top: 16.0),
          alignment: Alignment.center,
          child: ElevatedButton(
              onPressed: () async {
                //signInWithPhoneNumber();
              },
              child: Text("Sign in")),
        ),
      ],
    );
  }
}
