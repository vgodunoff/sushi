import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sms_autofill/sms_autofill.dart';
import 'package:sushi/bloc/auth/auth_bloc.dart';
import 'package:sushi/ui/widgets/alert_dialog_choice_city.dart';

class LoginScreenStfl extends StatefulWidget {
  @override
  State<LoginScreenStfl> createState() => _LoginScreenStflState();
}

class _LoginScreenStflState extends State<LoginScreenStfl> {
  final TextEditingController _phoneNumberController = TextEditingController();
  final TextEditingController _smsController = TextEditingController();
  final SmsAutoFill _autoFill = SmsAutoFill();
  static const routeName = '/profile';
  static const String prefixPhone = '+1';
  final FirebaseAuth _auth = FirebaseAuth.instance;
  String _verificationId = '';
  Future<void> verifyPhoneNumber(String phoneNumber) async {
// сначала опишем кол-бэки. они будут использованы ниже

// кол-бэк verificationCompleted отрабатывает когда подтвержден смс-код
    /* то есть сначала юзер вводит номер телефона, затем жмет кнопку и этот
    номер отправляется на сервер
  прилетает с сервера смс. если автоматически смс не проверился андроидом,
  то просим пользователя
  ввести этот смс и нажать кнопку - проверить
  когда проверка закончена и срабатывает кол-бэк verificationCompleted
  */
// PhoneAuthCredential phoneAuthCredential поставляется файербэйзом автоматически
    PhoneVerificationCompleted verificationCompleted =
        (PhoneAuthCredential phoneAuthCredential) async {
      await _auth.signInWithCredential(phoneAuthCredential);
      print(
          "Phone number automatically verified and user signed in: ${_auth.currentUser.uid}");
    };
// кол-бэк от сглаза
    PhoneVerificationFailed verificationFailed =
        (FirebaseAuthException authException) {
      print(
          'Phone number verification failed. Code: ${authException.code}. Message: ${authException.message}');
    };

    //Callback for when the code is sent
// String verificationId также прилетает от файербейза
//когда отработает этот кол-бэк то мы получим  от файербейза verificationId
// и запишем его в локальную переменную _verificationId
//  _verificationId используется для получения учетки в signInWithPhoneNumber
    PhoneCodeSent codeSent =
        (String verificationId, [int forceResendingToken]) async {
      _verificationId = verificationId;
      print('This is callback codeSent: '
          'Please check your phone for the verification code.'
          'verificationId from Firebase: $verificationId,'
          'local variable _verificationId: $_verificationId');
    };

    PhoneCodeAutoRetrievalTimeout codeAutoRetrievalTimeout =
        (String verificationId) {
      //print("verification code: " + verificationId);
      _verificationId = verificationId;
      print('This is callback codeAutoRetrievalTimeout: '
          'Please check your phone for the verification code.'
          'verificationId from Firebase: $verificationId,'
          'local variable _verificationId: $_verificationId');
    };

    try {
      print(phoneNumber);
      await _auth.verifyPhoneNumber(
          phoneNumber: phoneNumber,
          timeout: const Duration(seconds: 5),
          verificationCompleted: verificationCompleted,
          verificationFailed: verificationFailed,
          codeSent: codeSent,
          codeAutoRetrievalTimeout: codeAutoRetrievalTimeout);
    } catch (e) {
      print("Failed to Verify Phone Number: $e");
    }
  }

  Future<void> signInWithPhoneNumber(String smsCode) async {
    try {
      final AuthCredential credential = PhoneAuthProvider.credential(
        verificationId: _verificationId,
        smsCode: smsCode,
      );

      final User user = (await _auth.signInWithCredential(credential)).user;

      print("Successfully signed in UID: ${user.uid}");
    } catch (e) {
      print("Failed to sign in: " + e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        resizeToAvoidBottomInset: false,
        appBar: AppBar(
          title: Text('Авторизация'),
        ),
        backgroundColor: Colors.white,
        body: Padding(
          padding: const EdgeInsets.fromLTRB(8.0, 16.0, 8.0, 4.0),
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Text('Введите Ваш номер телефона'),
              ),
              TextField(
                textAlignVertical: TextAlignVertical.bottom,
                keyboardType: TextInputType.phone,
                maxLength: 10,
                autofocus: true,
                decoration: InputDecoration(
                  isDense: true,
                  icon: Text(
                    'Казахстан ${prefixPhone}',
                    style: TextStyle(color: Colors.red, fontSize: 16),
                  ),
                  filled: true,
                  fillColor: Colors.grey[100],
                  border: InputBorder.none,
                ),
                cursorColor: Colors.red,
                style: TextStyle(color: Colors.red),
                controller: _phoneNumberController,
              ),
              // ElevatedButton(
              //   onPressed: () async {
              //     _phoneNumberController.text = await _autoFill.hint;
              //   },
              //   child: Text('Get currrent number'),
              // ),
              TextButton(
                style: TextButton.styleFrom(
                  backgroundColor: Colors.red,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(12.0),
                  ),
                  minimumSize:
                      Size(MediaQuery.of(context).size.width * 0.8, 36),
                ),
                onPressed: () async {
                  String fullNumber = prefixPhone + _phoneNumberController.text;
                  // BlocProvider.of<AuthBloc>(context)
                  //     .add(LogIn(phoneNumber: fullNumber));
                  // showDialogChoiceCity(context);
                  verifyPhoneNumber(fullNumber);
                },
                child: Text(
                  'Verify number',
                  style: TextStyle(color: Colors.white),
                ),
              ),
              TextFormField(
                keyboardType: TextInputType.phone,
                controller: _smsController,
                decoration:
                    const InputDecoration(labelText: 'Verification code'),
              ),
              Container(
                padding: const EdgeInsets.only(top: 16.0),
                alignment: Alignment.center,
                child: ElevatedButton(
                    onPressed: () async {
                      signInWithPhoneNumber(_smsController.text);
                    },
                    child: Text("Sign in")),
              ),
            ],
          ),
        ));
  }
}

// Future<void> showDialogChoiceCity(BuildContext context) async {
//   return await showDialog(
//       context: context,
//       builder: (context) {
//         return ChoiceCity();
//       });
// }

class SignInWithOtp extends StatelessWidget {
  final TextEditingController _smsController = TextEditingController();

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        TextFormField(
          keyboardType: TextInputType.phone,
          controller: _smsController,
          decoration: const InputDecoration(labelText: 'Verification code'),
        ),
        Container(
          padding: const EdgeInsets.only(top: 16.0),
          alignment: Alignment.center,
          child: ElevatedButton(
              onPressed: () async {
                //signInWithPhoneNumber();
              },
              child: Text("Sign in")),
        ),
      ],
    );
  }
}
