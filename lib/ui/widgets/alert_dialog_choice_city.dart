import 'package:flutter/material.dart';

class ChoiceCity extends StatefulWidget {
  @override
  _ChoiceCityState createState() => _ChoiceCityState();
}

class _ChoiceCityState extends State<ChoiceCity> {
  String _value = 'Выберите город';

  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Text('Ваш город'),
        PopupMenuButton<String>(
          itemBuilder: (BuildContext context) => [
            PopupMenuItem(
              enabled: false,
              value: 'Выберите город',
              child: Text(
                "Выберите город",
              ),
            ),
            PopupMenuItem(
              value: "Алматы",
              child: Text(
                "Алматы",
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.w700),
              ),
            ),
            PopupMenuItem(
              value: "Flutter Tutorial",
              child: Text(
                "Flutter Tutorial",
                style:
                    TextStyle(color: Colors.black, fontWeight: FontWeight.w700),
              ),
            ),
          ],
          onSelected: (String value) {
            setState(() {
              _value = value;
            });
            print(_value);
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [Text(_value), Icon(Icons.arrow_drop_down)],
          ),
// offset: Offset(0, 100),
        ),
// DropdownButton(
//   isExpanded: true,
//   hint: Text(
//     'Выберите город',
//     style: TextStyle(color: Colors.black),
//   ),
//   items: [
//     DropdownMenuItem(
//       value: "1",
//       child: Text('Алматы'),
//     ),
//     DropdownMenuItem(
//       value: "2",
//       child: Text('Караганда'),
//     )
//   ],
//   onChanged: (value) {
//     _value = value;
//   },
//   value: _value,
// ),
      ],
    );
  }
}
