import 'package:flutter/material.dart';
import 'package:sushi/ui/widgets/buttons/button_want.dart';

class ButtonFavorite extends StatelessWidget {
  final bool inAppBar;

  const ButtonFavorite({
    Key key,
    @required this.inAppBar,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return IconButton(
      icon: Icon(
        Icons.favorite_border,
        color: inAppBar ? Colors.white : Colors.red,
        size: inAppBar ? 25 : 30,
      ),
      onPressed: () {
        WantButton.createDialog(context,
            'Чтобы добавить товар в список желаемого, пожалуйста авторизуйтесь');
      },
    );
  }
}
