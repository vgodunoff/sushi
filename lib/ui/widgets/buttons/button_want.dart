import 'package:flutter/material.dart';

class WantButton extends StatelessWidget {
  final bool smallButton;
  const WantButton({
    Key key,
    @required this.smallButton,
  }) : super(key: key);

  static createDialog(BuildContext context, String message) {
    return showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            content: Text(message),
            actions: [
              TextButton(
                  onPressed: () {
                    Navigator.of(context).pop();
                  },
                  child: Text(
                    'отмена'.toUpperCase(),
                    style: TextStyle(
                      color: Colors.red,
                    ),
                  )),
              TextButton(
                onPressed: () {
                  Navigator.of(context).pushNamed('/profile');
                },
                child: Text(
                  'ok'.toUpperCase(),
                  style: TextStyle(
                    color: Colors.red,
                  ),
                ),
              )
            ],
          );
        });
  }

  @override
  Widget build(BuildContext context) {
    return TextButton(
      style: TextButton.styleFrom(
          padding: smallButton
              ? EdgeInsets.symmetric(horizontal: 20)
              : EdgeInsets.symmetric(horizontal: 50, vertical: 6),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(18.0),
          ),
          primary: Colors.white,
          backgroundColor: Colors.red,
          textStyle: TextStyle(
            color: Colors.white,
          )),
      onPressed: () {
        createDialog(context,
            'Чтобы добавить товар в корзину, пожалуйста авторизуйтесь');
      },
      child: Row(
        children: [
          Text('Хочу'),
          SizedBox(
            width: 5,
          ),
          Icon(
            Icons.add_shopping_cart,
            size: smallButton ? 15 : 25,
          )
        ],
      ),
    );
  }
}
