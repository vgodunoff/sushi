import 'package:flutter/material.dart';

class ProductImage extends StatelessWidget {
  ProductImage({@required this.productData, this.isDetail});

  final Map<String, dynamic> productData;
  final bool isDetail;

  @override
  Widget build(BuildContext context) {
    return Container(
      constraints: BoxConstraints(
        maxHeight: isDetail ? 220 : 80,
        maxWidth: isDetail ? 370 : 100,
      ),
      decoration: BoxDecoration(
        shape: BoxShape.rectangle,
        borderRadius: BorderRadius.all(Radius.circular(10.0)),
        image: DecorationImage(
            image: NetworkImage(productData['imageUrl']), fit: BoxFit.cover),
      ),
    );
  }
}
