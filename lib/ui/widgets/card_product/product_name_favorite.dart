import 'package:flutter/material.dart';
import 'package:sushi/ui/widgets/buttons/button_favorite.dart';

import '../buttons/button_want.dart';

class ProductNameAndFavorite extends StatelessWidget {
  const ProductNameAndFavorite({
    Key key,
    @required this.productData,
  }) : super(key: key);

  final Map<String, dynamic> productData;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisSize: MainAxisSize.max,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Flexible(
          child: Text(
            productData['name'] +
                ',  Long long text, the name of sushi set (1990 г.)',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.w500, fontSize: 15),
            softWrap: true,
            maxLines: 5,
            overflow: TextOverflow.ellipsis,
          ),
        ),
        ButtonFavorite(
          inAppBar: false,
        ),
      ],
    );
  }
}
