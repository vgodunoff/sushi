import 'package:flutter/material.dart';

import '../buttons/button_want.dart';

class ProductPriceOrder extends StatelessWidget {
  final Map<String, dynamic> productData;
  ProductPriceOrder({@required this.productData});

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Text(
          '${productData['price'].toString()},00 тг.',
          style: TextStyle(
              color: Colors.red, fontWeight: FontWeight.w600, fontSize: 15),
        ),
        WantButton(smallButton: true),
      ],
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
    );
  }
}
