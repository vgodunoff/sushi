import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:sushi/bloc/dashboard_bloc/dash_bloc.dart';
import 'package:sushi/ui/icons/trolley_cart_icons.dart';

class SushiBottomNavigationBar extends StatelessWidget {
  final int index;
  const SushiBottomNavigationBar({
    Key key,
    this.index,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      type: BottomNavigationBarType.fixed,
      onTap: (index) {
        BlocProvider.of<DashBloc>(context)
            .add(ShowPage(currentSelected: index));
      },
      currentIndex: index,
      unselectedItemColor: Colors.grey,
      selectedItemColor: Colors.blue,
      items: [
        BottomNavigationBarItem(
          icon: Icon(Icons.menu),
          label: 'Меню',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.shopping_cart_outlined),
          label: 'Корзина',
        ),
        BottomNavigationBarItem(
          icon: Icon(TrolleyCart.trolley),
          label: 'Мои заказы',
        ),
        BottomNavigationBarItem(
          icon: Icon(Icons.settings_outlined),
          label: 'Настройки',
        ),
      ],
    );
  }
}
