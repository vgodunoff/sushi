import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/svg.dart';
import 'package:sushi/bloc/dashboard_bloc/dash_bloc.dart';
import 'package:sushi/repository/constants.dart';
import 'package:sushi/ui/icons/custom_icon_icons.dart';
import 'package:sushi/ui/screens/cart.dart';
import 'package:sushi/ui/screens/main_page/menu/goods.dart';
import 'package:sushi/ui/screens/my_orders.dart';
import 'package:sushi/ui/screens/settings.dart';

import 'package:sushi/ui/widgets/widgets_home_page/bottom_navigation_bar.dart';

class DashboardHomeScreen extends StatelessWidget {
  final GlobalKey<ScaffoldState> _drawerKey = GlobalKey();

  @override
  Widget build(BuildContext context) {
    return BlocProvider(
      create: (context) => DashBloc()..add(ShowPage(currentSelected: 0)),
      child: BlocBuilder<DashBloc, DashState>(
        builder: (context, state) {
          if (state is FirstPageSelected) {
            return SafeArea(
              child: Scaffold(
                key: _drawerKey,
                drawer: MyDrawer(),
                bottomNavigationBar: SushiBottomNavigationBar(
                  index: state.currentIndex,
                ),
                body: Goods(
                  collectionStream: state.collectionStream,
                  title: state.title,
                ),
              ),
            );
          }
          if (state is SecondPageSelected) {
            return SafeArea(
              child: Scaffold(
                key: _drawerKey,
                drawer: MyDrawer(),
                bottomNavigationBar: SushiBottomNavigationBar(
                  index: state.currentIndex,
                ),
                body: Cart(),
              ),
            );
          }
          if (state is ThirdPageSelected) {
            return SafeArea(
              child: Scaffold(
                key: _drawerKey,
                drawer: MyDrawer(),
                bottomNavigationBar: SushiBottomNavigationBar(
                  index: state.currentIndex,
                ),
                body: MyOrders(),
              ),
            );
          }
          if (state is FourthPageSelected) {
            return SafeArea(
              child: Scaffold(
                key: _drawerKey,
                drawer: MyDrawer(),
                bottomNavigationBar: SushiBottomNavigationBar(
                  index: state.currentIndex,
                ),
                body: Settings(),
              ),
            );
          }
          return const SizedBox.shrink();
        },
      ),
    );
  }
}

class MyDrawer extends StatelessWidget {
  const MyDrawer({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.all(16.0),
      width: MediaQuery.of(context).size.width * 0.65,
      height: MediaQuery.of(context).size.height,
      color: Colors.white,
      child: ListView(
        children: [
          Wrap(
            children: [
              Column(
                children: [
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Expanded(
                        flex: 1,
                        child: Column(
                          children: [
                            InkWell(
                              child: Icon(
                                CustomIcon.set_icon,
                                size: 35,
                              ),
                              onTap: () {
                                BlocProvider.of<DashBloc>(context)
                                    .add(ChangeFirstPage(
                                  // collectionStream: collectionSet,
                                  title: 'Сеты',
                                ));
                                //changeFirstPage(SetsScreen());
                              },
                            ),
                            Text('')
                          ],
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: InkWell(
                          child: Container(
                            child: SvgPicture.asset('images/maki.svg'),
                            height: 35.0,
                          ),
                          onTap: () {
                            BlocProvider.of<DashBloc>(context).add(
                              ChangeFirstPage(
                                  // collectionStream: collectionRoll,
                                  title: 'Роллы'),
                            );
                            //changeFirstPage(Rolls());
                          },
                        ),
                      ),
                      Expanded(
                        flex: 1,
                        child: Icon(
                          CustomIcon.set_icon,
                          size: 50,
                        ),
                      )
                    ],
                  ),
                  SizedBox(
                    height: 16.0,
                  ),
                ],
              )
            ],
          )
        ],
      ),
    );
  }
}
